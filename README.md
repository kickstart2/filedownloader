# Downloader util

- pause/resume/delete downloads
- no 3rd party libraries
- demo app with audio + video content
- integration tests for basic features

Inspired by Coding Test from [SanggeonPark](https://github.com/SanggeonPark/FileDownloaderCodingTest)
