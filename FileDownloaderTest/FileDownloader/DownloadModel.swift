//
//  DownloadModel.swift
//  FileDownloaderTest
//
//  Created by Sanggeon Park on 13.06.19.
//  Copyright © 2019 Sanggeon Park. All rights reserved.
//

import Foundation

public enum DownloadStatus: String, Codable {
    case NONE
    case WAITING
    case DOWNLOADING
    case PAUSED
    case DOWNLOADED
}

public struct DownloadModel: Codable {
    public let identifier: String
    public let status: DownloadStatus
    public let progress: Float // from 0.0% to 100.0%
    public let remoteFilePath: String
    public let localFilePath: String?
}

extension DownloadModel {
    init(identifier: String, remoteFilePath: String) {
        self.identifier = identifier
        self.remoteFilePath = remoteFilePath
        status = .NONE
        progress = 0
        localFilePath = nil
    }
    
    func with(status: DownloadStatus) -> DownloadModel {
        return DownloadModel(identifier: self.identifier, status: status, progress: self.progress, remoteFilePath: self.remoteFilePath, localFilePath: self.localFilePath)
    }
    
    func with(localFilePath: String) -> DownloadModel {
        return DownloadModel(identifier: self.identifier, status: self.status, progress: self.progress, remoteFilePath: self.remoteFilePath, localFilePath: localFilePath)
    }
    
    func with(progress: Float) -> DownloadModel {
        return DownloadModel(identifier: self.identifier, status: self.status, progress: progress, remoteFilePath: self.remoteFilePath, localFilePath: self.localFilePath)
    }
    
    func with(localPath: String) -> DownloadModel {
        return DownloadModel(identifier: self.identifier, status: self.status, progress: self.progress, remoteFilePath: remoteFilePath, localFilePath: localPath)
    }
}

extension DownloadModel: Equatable {
    static public func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.identifier == rhs.identifier &&
            lhs.progress == rhs.progress &&
            lhs.status == rhs.status
    }
}
