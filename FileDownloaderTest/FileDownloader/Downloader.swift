//
//  Downloader.swift
//  FileDownloaderTest
//
//  Created by Sanggeon Park on 13.06.19.
//  Copyright © 2019 Sanggeon Park. All rights reserved.
//

import Foundation

enum DownloaderError: LocalizedError {
    case invalidPath
    case invalidId
    case fileNotFound
    case noActionApplied
}

public protocol DownloaderDelegate: class {
    func didUpdateDownloadStatus(for identifier: String, progress: Float, status: DownloadStatus, error: Error?)
}

extension DownloaderDelegate {
    func didUpdateDownloadStatus(for identifier: String, progress: Float, status: DownloadStatus, error: Error?) {
        // Optional Function
    }
}

open class Downloader: NSObject {
    weak var delegate: DownloaderDelegate?
    
    private var unfinishedData: [String: Data]
    private var downloads: [String: DownloadModel]
    private var tasks: [URLSessionDownloadTask: String]
    private lazy var session: URLSession = URLSession.init(configuration: .default, delegate: self, delegateQueue: nil)

    public init(with delegate: DownloaderDelegate? = nil) {
        self.delegate = delegate
        unfinishedData = [String: Data]()
        downloads = [String: DownloadModel]()
        tasks = [URLSessionDownloadTask: String]()
    }

    public func allDownloads(_ completion: @escaping ([DownloadModel]?) -> Void) {
        DispatchQueue.main.async {
            completion(self.downloads.map({ $0.value }))
        }
    }

    public func resumeDownload(for identifier: String, remotePath: String,
                        _ completion: @escaping (_ data: DownloadModel?, _ error: Error?) -> Void) {
        var resumeModel: DownloadModel?
        var resumeError: Error?
        do {
            let model = modelFor(identifier, with: remotePath)
            switch model.status {
            case .PAUSED, .NONE:
                let task = try getTask(with: identifier, remotePath: remotePath)
                task.resume()
                resumeModel = model.with(status: .DOWNLOADING)
                updateModel(identifier, with: resumeModel!)
            case .DOWNLOADED:
                resumeModel = model.with(progress: 100.0)
                updateModel(identifier, with: resumeModel!)
            default:
                resumeError = DownloaderError.noActionApplied
            }
        } catch {
            resumeError = error
        }
        
        DispatchQueue.main.async {
            completion(resumeModel, resumeError)
        }
    }

    public func pauseDownload(for identifier: String, _ completion: @escaping (_ data: DownloadModel?, _ error: Error?) -> Void) {
        var pauseModel: DownloadModel?
        var pauseError: Error?
        if let model = modelFor(identifier) {
            if model.status == .DOWNLOADING, let task = getTask(for: identifier) {
                task.cancel(byProducingResumeData: { self.unfinishedData[identifier] = $0 })
                deleteTask(task)
                pauseModel = model.with(status: .PAUSED)
                updateModel(identifier, with: pauseModel!)
            } else {
                pauseModel = model
            }
        } else {
            pauseError = DownloaderError.invalidId
        }
        
        DispatchQueue.main.async {
            completion(pauseModel, pauseError)
        }
    }

    public func removeDownload(for identifier: String, _ completion: @escaping (_ error: Error?) -> Void) {
        var deleteError: Error? = DownloaderError.fileNotFound
        
        if let model = modelFor(identifier) {
            if let path = model.localFilePath, model.status == .DOWNLOADED {
                removeFile(at: path)
            }
            updateModel(identifier, with: DownloadModel.init(identifier: model.identifier, remoteFilePath: model.remoteFilePath))
            removeModel(identifier)
            deleteError = nil
        } else {
            deleteError = DownloaderError.invalidId
        }
        
        DispatchQueue.main.async {
            completion(deleteError)
        }
    }

    public func downloadData(for identifier: String, _ completion: @escaping (_ data: DownloadModel?) -> Void) {
        DispatchQueue.main.async {
            completion(nil)
        }
    }
    
    deinit {
        tasks.keys.forEach( { $0.cancel() } )
        print("deinit")
    }
}


// MARK: - URLSessionDownloadDelegate

extension Downloader: URLSessionDownloadDelegate {
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let model = modelForTask(downloadTask)
        let remoteFilePath = URL(string: model.remoteFilePath)!
        let filePath = localFileURL(for: remoteFilePath, with: model.identifier)
        copyFile(at: location, to: filePath)
        updateModel(model.identifier, with: model.with(status: .DOWNLOADED).with(localFilePath: filePath.path))
        deleteTask(downloadTask)
    }
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let model = modelForTask(downloadTask)
        updateModel(model.identifier, with: model.with(progress: floor(Float(totalBytesWritten) * 1000 / Float(totalBytesExpectedToWrite)) / 10))
    }
    
    public func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        print("didBecomeInvalidWithError")
    }
}


// MARK: - Manage DownloadTasks

private extension Downloader {
    func getTask(with identifier: String, remotePath: String) throws -> URLSessionDownloadTask {
        
        // MARK: Get task with resume data
        if let resumeData = unfinishedData[identifier] {
            unfinishedData.removeValue(forKey: identifier)
            let task = createTask(for: resumeData)
            tasks[task] = identifier
            return task
        }
        
        // MARK: Get existing task
        if let task = getTask(for: identifier) {
            return task
        }
        
        // MARK: Get new task
        do {
            let task = try createTask(for: identifier, remotePath: remotePath)
            tasks[task] = identifier
            return task
        } catch {
            throw error
        }
    }
    
    func getTask(for id: String) -> URLSessionDownloadTask? {
        return tasks.filter( { $0.value == id } ).first?.key
    }
    
    func createTask(for id: String, remotePath: String) throws -> URLSessionDownloadTask {
        guard let url = URL(string: remotePath) else {
            throw DownloaderError.invalidPath
        }
        let task = session.downloadTask(with: url)
        return task
    }
    
    func createTask(for resumeData: Data) -> URLSessionDownloadTask {
        return session.downloadTask(withResumeData: resumeData)
    }
    
    func deleteTask(_ task: URLSessionDownloadTask) {
        tasks.removeValue(forKey: task)
    }
}

// MARK: - Manage DownloadModels
private extension Downloader {
    
    func modelForTask(_ task: URLSessionDownloadTask) -> DownloadModel {
        guard let ideintifier = tasks[task], let model = downloads[ideintifier] else {
            fatalError("modelForTask: No id linked to task")
        }
        return model
    }
    
    func modelFor(_ identifier: String) -> DownloadModel? {
        guard let model = downloads[identifier] else {
            return nil
        }
        return model
    }
    
    func modelFor(_ identifier: String, with remotePath: String) -> DownloadModel {
        guard let model = modelFor(identifier) else {
            var model = DownloadModel.init(identifier: identifier, remoteFilePath: remotePath)
   
            // TODO: you can use following block to check whether file was previously stored to the device
//            if let url = URL(string: remotePath) {
//                let localURL = localFileURL(for: url, with: identifier)
//                if FileManager.default.isReadableFile(atPath: localURL.path) {
//                    model = model.with(status: .DOWNLOADED).with(localFilePath: localURL.path)
//                }
//            }
            downloads[identifier] = model
            return model
        }
        return model
    }
    
    func updateModel(_ identifier: String, with model: DownloadModel, error: Error? = nil) {
        let oldModel = downloads[identifier]
        downloads[identifier] = model
        if (oldModel != model) {
            DispatchQueue.main.async {
                self.delegate?.didUpdateDownloadStatus(for: model.identifier, progress: model.progress, status: model.status, error: error)
            }
        }
    }
    
    func removeModel(_ identifier: String) {
        downloads.removeValue(forKey: identifier)
    }
}


// MARK: - File operations
private extension Downloader {
    func localFileURL(for sourceUrl: URL, with identifier: String) -> URL {
        let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        // workaround for missing file name extension to play video with AVKit
        let fileExt = sourceUrl.pathExtension.isEmpty ? ".m4v" : ""
        guard let destination = documents?.appendingPathComponent("\(identifier)-\(sourceUrl.lastPathComponent)\(fileExt)") else {
            fatalError("Unable to generate URL for destination file \(identifier)-\(sourceUrl.lastPathComponent)\(fileExt)")
        }
        return destination
    }
    
    func copyFile(at location: URL, to destination: URL) {
        removeFile(at: destination.path)
        do {
            try FileManager.default.copyItem(at: location, to: destination)
        } catch {
            fatalError("Unable to copy file \(error.localizedDescription)")
        }
//        print(FileManager.default.urls(for: .documentDirectory) ?? "none")
    }
    
    func removeFile(at path: String) {
        try? FileManager.default.removeItem(atPath: path)
    }
}

extension FileManager {
    func urls(for directory: FileManager.SearchPathDirectory, skipsHiddenFiles: Bool = true ) -> [URL]? {
        let documentsURL = urls(for: directory, in: .userDomainMask)[0]
        let fileURLs = try? contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [] )
        return fileURLs
    }
}

