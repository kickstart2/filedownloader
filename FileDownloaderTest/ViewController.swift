//
//  ViewController.swift
//  FileDownloaderTest
//
//  Created by Sanggeon Park on 13.06.19.
//  Copyright © 2019 Sanggeon Park. All rights reserved.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    @IBOutlet weak var tvDownloads: UITableView!
    
    let viewModels = [DownloadViewModel(with: "Video 1", remotePath: "https://media.istockphoto.com/videos/drone-point-view-of-city-street-crossing-at-night-video-id1170985573?b=1&k=6&m=1170985573&h=-p4EpBW8kF2YQDimnfQsQgGTQithWOSw4kQ-X1THhL0="),
                      DownloadViewModel(with: "Video 2", remotePath: "https://media.istockphoto.com/videos/drone-view-of-hong-kong-kowloon-city-skyline-at-night-video-id1215054627?b=1&k=6&m=1215054627&h=8SOV9Ik9Jp7W5aAzRcwDrdKnwrtOgBHrQci8aDZJTiQ="),
                      DownloadViewModel(with: "Image 3", remotePath: "https://images.pexels.com/photos/373912/pexels-photo-373912.jpeg?cs=srgb&dl=pexels-burst-373912.jpg&fm=jpg")]

    var downloader = Downloader()

    override func viewDidLoad() {
        super.viewDidLoad()
        downloader.delegate = self
    }
}

extension ViewController: DownloaderDelegate {
    func didUpdateDownloadStatus(for identifier: String, progress: Float, status: DownloadStatus, error: Error?) {
//        print("didUpdateDownloadStatus \([identifier, progress, status, error]) ")
        if let index = viewModels.firstIndex( where: { $0.identifier == identifier } ) {
            let viewModel = viewModels[index]
            viewModel.progress = progress
            viewModel.downloadStatus = status
            tvDownloads.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        cell.textLabel?.text = viewModels[indexPath.row].identifier
        let status = viewModels[indexPath.row].downloadStatus
        switch status {
        case .DOWNLOADING:
            cell.detailTextLabel?.text = "\(viewModels[indexPath.row].progress)%"
        default:
            cell.detailTextLabel?.text = viewModels[indexPath.row].downloadStatus.rawValue
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let status = viewModels[indexPath.row].downloadStatus
        switch status {
        case .NONE,
             .PAUSED:
            downloader.resumeDownload(for: viewModels[indexPath.row].identifier, remotePath: viewModels[indexPath.row].remoteFilePath) { _, _ in }
            break
        case .DOWNLOADING:
            downloader.pauseDownload(for: viewModels[indexPath.row].identifier) { _, _ in }
            break
        case .DOWNLOADED:
            showContent(viewModels[indexPath.row].identifier)
            break
        default:
            break
        }

        tableView.deselectRow(at: indexPath, animated: false)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            downloader.removeDownload(for: viewModels[indexPath.row].identifier) { error in
                print("removeDownload \(error?.localizedDescription ?? "OK")") }
        }
    }
}

private extension ViewController {
    func showContent(_ identifier: String) {
//        print("PLAY DOWNLOADED VIDEO \(identifier)")
        downloader.allDownloads { models in
            guard let localPath = models?.filter( { $0.identifier == identifier && [DownloadStatus.DOWNLOADED].contains($0.status) } ).first?.localFilePath else {
                fatalError("Content for \(identifier) not found")
            }
            print("PLAY \(localPath)")
            let url = URL(fileURLWithPath: localPath)
            switch url.pathExtension {
            case "jpg", "jpeg":
                self.showImage(url)
            case "m4v", "mp4":
                self.showVideo(url)
            default:
                print("Unsupported format \(url.pathExtension)")
            }
        }
    }
    
    func showVideo(_ url: URL) {
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        vc.player = player

        self.present(vc, animated: true) {
            vc.player?.play()
        }
    }
    
    func showImage(_ url: URL) {
        let vImg = UIImageView(image: UIImage(contentsOfFile: url.path))
        vImg.contentMode = .scaleAspectFit
        vImg.frame = CGRect.init(x: 0, y: view.frame.height - 300, width: view.frame.width, height: 300)
        self.view.addSubview(vImg)
    }
}

